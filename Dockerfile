FROM openjdk:8u342-slim
ARG JAR_FILE
COPY target/${JAR_FILE} cicdtest.jar
CMD ["java", "-jar","cicdtest.jar"]