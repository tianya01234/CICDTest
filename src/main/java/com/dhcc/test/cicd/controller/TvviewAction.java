package com.dhcc.test.cicd.controller;

import com.dhcc.test.cicd.domain.Tvview;
import com.dhcc.test.cicd.service.TvviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/tvview")
public class TvviewAction {
    @Autowired
    TvviewService tvviewService;

    @GetMapping
    public List<Tvview> getAll() {
        return tvviewService.list();
    }
}
