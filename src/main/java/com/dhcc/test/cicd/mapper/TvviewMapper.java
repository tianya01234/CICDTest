package com.dhcc.test.cicd.mapper;

import com.dhcc.test.cicd.domain.Tvview;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Majianzhi
* @description 针对表【tvview】的数据库操作Mapper
* @createDate 2022-08-19 15:22:18
* @Entity com.dhcc.test.cicd.domain.Tvview
*/
public interface TvviewMapper extends BaseMapper<Tvview> {

}




