package com.dhcc.test.cicd.service;

import com.dhcc.test.cicd.domain.Tvview;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Majianzhi
* @description 针对表【tvview】的数据库操作Service
* @createDate 2022-08-19 15:22:18
*/
public interface TvviewService extends IService<Tvview> {

}
