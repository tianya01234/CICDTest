package com.dhcc.test.cicd.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dhcc.test.cicd.domain.Tvview;
import com.dhcc.test.cicd.service.TvviewService;
import com.dhcc.test.cicd.mapper.TvviewMapper;
import org.springframework.stereotype.Service;

/**
* @author Majianzhi
* @description 针对表【tvview】的数据库操作Service实现
* @createDate 2022-08-19 15:22:18
*/
@Service
public class TvviewServiceImpl extends ServiceImpl<TvviewMapper, Tvview>
    implements TvviewService{

}




